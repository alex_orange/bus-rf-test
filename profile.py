#!/usr/bin/python

"""
This profile is for testing buses for RF. Instantiate it, then run bus.py
while transmitting from the laptop, check the values (TODO: Update with
expected values), then run laptop.py on the laptop while transmitting from the
bus and check the values.


Instructions:


**Test laptop to bus**

Run on the laptop:

    uhd_siggen -f ...

Run on the bus:

    python3 bus.py

Check that the value is between XXX and YYY.


Run on the bus:

    uhd_siggen -f ...

Run on the laptop:

    python3 laptop.py

Check that the value is between XXX and YYY.


"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


b210_node_disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"

setup_command = "/local/repository/startup.sh"


def b210_nuc_pair(idx, b210_node):
    node_name = b210_node.node_name.format(idx=idx)
    b210_nuc_pair_node = request.RawPC(b210_node.node_name.format(
        idx=idx))
    b210_nuc_pair_node.component_manager_id = b210_node.aggregate_id
    b210_nuc_pair_node.component_id = b210_node.component_id

    b210_nuc_pair_node.disk_image = b210_node_disk_image

    b210_nuc_pair_node.addService(
        rspec.Execute(shell="bash", command=setup_command))




fixed_endpoint_aggregates = [
    ("urn:publicid:IDN+web.powderwireless.net+authority+cm",
     "Warnock Engineering Building"),
    ("urn:publicid:IDN+ebc.powderwireless.net+authority+cm",
     "Eccles Broadcast Center"),
    ("urn:publicid:IDN+bookstore.powderwireless.net+authority+cm",
     "Bookstore"),
    ("urn:publicid:IDN+humanities.powderwireless.net+authority+cm",
     "Humanities"),
    ("urn:publicid:IDN+law73.powderwireless.net+authority+cm",
     "Law (building 73)"),
    ("urn:publicid:IDN+madsen.powderwireless.net+authority+cm",
     "Madsen Clinic"),
    ("urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm",
     "Sage Point"),
    ("urn:publicid:IDN+moran.powderwireless.net+authority+cm",
     "Moran Eye Center"),
]


portal.context.defineStructParameter("b210_nodes", "Add Node", [],
                                     multiValue=True,
                                     itemDefaultValue=
                                     {"component_id": "ed1",
                                      "aggregate_id": "urn:publicid:...",
                                      "node_name": "b210-node-{idx}"},
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "component_id",
                                             "Component ID (like ed1)",
                                             portal.ParameterType.STRING, ""),
                                         portal.Parameter(
                                             "aggregate_id",
                                             "Fixed Endpoint",
                                             portal.ParameterType.STRING, ""),
                                         portal.Parameter(
                                             "node_name",
                                             "Node name, will be .format'd "
                                             "with idx",
                                             portal.ParameterType.STRING,
                                             "")
                                     ],
                                    )



params = portal.context.bindParameters()

request = portal.context.makeRequestRSpec()


for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, b210_node)


portal.context.printRequestRSpec()

