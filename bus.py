import uhd
import numpy
import cmath
import math
import sys


frequency = 2625e6
offset = 30e3


try:
    device = uhd.usrp.MultiUSRP()
except RuntimeError as e:
    message = e.args[0]
    if message == "LookupError: KeyError: No devices found for ----->\nEmpty Device Address":
        print("... engage telepathy ... FAIL ... look for SDR ... FAIL")
        print("Trying plugging in the SDR")
        sys.exit(1)
    else:
        raise e

if device.get_mboard_name() != "B210":
    print("... This is not the radio you are looking for ... FAIL")
    print("Weakminded one")
    sys.exit(1)


samp_rate = 200e3

for i in range(2):
	device.set_rx_rate(samp_rate, i)


n_rx = 2**14
n_flush = 2**20


stream_args = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
stream_args.channels = [0]

rx_stream = device.get_rx_stream(stream_args)

rx_buff = numpy.zeros((1, n_rx), dtype=numpy.complex64)
rx_tmp_buff = numpy.zeros((1, rx_stream.get_max_num_samps()),
						  dtype=numpy.complex64)

metadata = uhd.libpyuhd.types.rx_metadata()

stream_cmd = uhd.libpyuhd.types.stream_cmd(
        uhd.libpyuhd.types.stream_mode.num_done)
stream_cmd.num_samps = n_rx
stream_cmd.stream_now = False

flush_stream_cmd = uhd.libpyuhd.types.stream_cmd(
        uhd.libpyuhd.types.stream_mode.num_done)
flush_stream_cmd.num_samps = n_flush
flush_stream_cmd.stream_now = False

fail_stream_cmd = uhd.libpyuhd.types.stream_cmd(
        uhd.libpyuhd.types.stream_mode.stop_cont)


device.set_rx_antenna("TX/RX", 0)
device.set_rx_gain(73, 0)

tune_request = uhd.types.TuneRequest(frequency)
for i in range(2):
    device.set_rx_freq(tune_request, i)


sdr_time = device.get_time_now().get_real_secs()

j = 0

flush_stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(sdr_time + 6e-3)
rx_stream.issue_stream_cmd(flush_stream_cmd)

while j < n_flush:
    read_count = rx_stream.recv(rx_tmp_buff, metadata, 0.1)
    if metadata.error_code != uhd.libpyuhd.types.rx_metadata_error_code.none:
        print(j)
    j += read_count


for i in range(10):
    rx_buff = numpy.zeros((1, n_rx), dtype=numpy.complex64)
    j = 0

    sdr_time = device.get_time_now().get_real_secs()

    stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(sdr_time + 6e-3)
    rx_stream.issue_stream_cmd(stream_cmd)

    while j < n_rx:
        read_count = rx_stream.recv(rx_tmp_buff, metadata, 0.1)
        if metadata.error_code != uhd.libpyuhd.types.rx_metadata_error_code.none:
            print(j)
        rx_buff[:, j:j+read_count] = rx_tmp_buff[:, 0:read_count]
        j += read_count


    shifted_rx_buff = [cmath.exp(-1j*2*math.pi*i*offset/samp_rate) * _
                       for i, _ in enumerate(rx_buff[0, :])]

    energy = 0
    for i in range(len(shifted_rx_buff)-9):
        vector = 0
        for j in range(10):
            vector += shifted_rx_buff[i+j]
        energy += abs(vector)**2

    power = energy / (len(shifted_rx_buff)-9)

    print(power, energy)
